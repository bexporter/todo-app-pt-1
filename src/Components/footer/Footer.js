
import React from 'react';
import {NavLink} from 'react-router-dom'
const Footer= (props)=>{


    
      

return(
<footer className="footer">
  {/* <!-- This should be `0 items left` by default --> */}
  <span className= "todo-count">
    <strong>0</strong> item(s) left
  </span>
  <ul className="filters">
    <li>
      <NavLink to="/">All</NavLink>
    </li>
    <li>
      <NavLink to="/active">Active</NavLink>
    </li>
    <li>
      <NavLink to="/completed">Completed</NavLink>
    </li>
  </ul>
  < button className="clear-completed" onClick= {props.handleClearAll}>Clear completed</button>

</footer>
)
}

export default Footer



      
          
          