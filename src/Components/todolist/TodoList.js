import React, { Component } from "react";
import TodoItem from '../todoItem/TodoItem'

class TodoList extends Component {
   
    render() {
      return (
        <section className="main">
          <ul className="todo-list">
            {this.props.todos.map((todo) => (
              <TodoItem handleToggle={this.props.handleToggle} 
              id ={todo.id} 
              title={todo.title} 
              completed={todo.completed} 
              handleDelete ={this.props.handleDelete}
              />
            ))}
          </ul>
        </section>
      );
    }
  }

  export default TodoList;