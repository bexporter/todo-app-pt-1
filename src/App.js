import React, { Component } from "react";
import todosList from "./todos.json";
import TodoList from './Components/todolist/TodoList';
import Footer from "./Components/footer/Footer";
import {Switch, Route} from "react-router-dom";
class App extends Component {
  state = {
    todos: todosList,
  };
  handleSubmit = (e)=>{
    if (e.key==='Enter'){
      let newTodo = {
          "userId": 1,
          "id": Math.random()*12000,
          "title": e.target.value,
          "completed": false
        }
        console.log(newTodo)
        this.setState((state)=>({todos:[...state.todos, newTodo]}))
      }
    
  }
handleToggle=(id)=>{
  let newTodos  = this.state.todos.map(todo=>{
    if(todo.id === id){
      return {...todo, completed:!todo.completed}
    }
    return todo
  })
   this.setState(state=>({todos:newTodos}))
}

handleDelete= (id) =>{
  let killTodo = this.state.todos.filter(todo=>{
    console.log(id)
    if(todo.id !==id){
      return todo
    }
  
  } )
  this.setState(state=>({todos: killTodo}))
}
  
handleClearAll = () =>{
  console.log("test")
  let clearAll = this.state.todos.filter(todo=>{
   if (todo.completed === false){
      return todo 
  }
  })
  this.setState(state=>({todos:clearAll}))
}

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input className="new-todo" placeholder="What needs to be done?" autofocus onKeyDown= {this.handleSubmit}/>
        </header>
        <Switch>
        <Route exact
        path= "/">
          <TodoList todos={this.state.todos} handleToggle={this.handleToggle} handleDelete= {this.handleDelete}/>
        </Route>
        <Route exact
        path= "">
          <TodoList todos={this.state.todos.filter(todo=>todo.title==="
          ")} handleToggle={this.handleToggle} handleDelete= {this.handleDelete}/>
        </Route>
        <Route 
        path= "/active">
          I need to render my todo list, but the todos that I give it need to be filtered so that only the ones NOT completed get passed in
        </Route>
        <Route path= "/completed">
          I need to render my todolist so that only the ones ARE completed will be rendered
        </Route>
        </Switch>
        <Footer todos= {this.state.todos} handleClearAll= {this.handleClearAll}/>
      </section>
    );
  }
}





export default App;


//Thanks to Suri and Vincent Newsome for amazing assistance!!